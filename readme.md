# Simple i memory engines tester using Docker & NodeJS

## Requirements
- Docker & docker compose

## Running tests
`$ sh run.sh`

Results from various engines are stored in results directory
