#!/bin/bash

declare engines=(memory aerospike mongodb redis memcached)
declare requests=10000
declare concurrency=100

function wait_for_containers {
  while ! curl 127.0.0.1:8081/health &>/dev/null
  do
    sleep 1
  done
  echo "containers up, starting tests"
}

function do_ab {
  ab -n "$requests" -c "$concurrency" "127.0.0.1:8081/$1" > "results/$1.txt"
}

function do_tests {
  echo "Starting tests for $1"
  docker-compose up -d "$1" &>/dev/null
  wait_for_containers
  do_ab "$1"
  docker-compose down &>/dev/null
  echo "Tests complete for $1"
}

for engine in "${engines[@]}"
do
  do_tests "$engine"
done

