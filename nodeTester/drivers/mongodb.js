const { MongoClient } = require('mongodb');

const url = 'mongodb://mongo:27017';
const dbName = 'testing';
const collection = 'documents';
let client = null;

const getClient = async () => {
    if (!client) {
        client = await new Promise((resolve, reject) => {
            MongoClient.connect(url, (err, client) => {
                if (err === null) {
                    resolve(client);
                    return;
                }
                reject(err);
            });
        });
    }

    return client;
};

const mongoDbDriver = {
    set: async (key, value) => {
        client = await getClient();
        await client.db(dbName).collection(collection).insertOne({
            _id: key,
            value
        });
    },
    getAll: async () => {
        client = await getClient();
        client.db(dbName);
        return client.db(dbName).collection(collection).count();
    }
};

module.exports = mongoDbDriver;
