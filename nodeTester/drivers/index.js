const memory = require('./memory');
const mongodb = require('./mongodb');
const redis = require('./redis');
const memcached = require('./memcached');
const aerospike = require('./aerospike');

module.exports = {
	memcached,
	memory,
	mongodb,
	redis,
	aerospike
};
