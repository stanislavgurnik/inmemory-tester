const Memcached = require('memcached');
const memcached = new Memcached(['memcached']);

const memcachedDriver = {
    set: async (key, value) => {
        return new Promise((resolve, reject) => {
            memcached.add(key, value, 100, (err) => {
                if (!err) {
                    resolve();
                    return;
                }

                reject(err);
            });
        });
    },
    getAll: async () => {
        return new Promise((resolve, reject) => {
            memcached.stats((err, res) => {
                if (!err) {
                    resolve(res[0].total_items);
                    return;
                }

                reject(err);
            });
        });
    }
};

module.exports = memcachedDriver;
