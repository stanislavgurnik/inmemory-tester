'use strict';

const simpleDict = {};

const memoryDriver = {
    set: async (key, value) => {
        simpleDict[key] = value;
    },
    getAll: async () => {
        return Object.keys(simpleDict).length;
    }
};

module.exports = memoryDriver;
