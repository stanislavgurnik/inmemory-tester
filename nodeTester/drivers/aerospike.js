const Aerospike = require('aerospike');

const globalKey = new Aerospike.Key('test', 'foo', 'bar');

let client = null;
const getClient = async () => {
    if (!client) {
        client = await Aerospike.connect({
            hosts: 'aerospike:3000'
        });

        await client.put(globalKey, { map: {} }, { ttl: 100000 }, new Aerospike.WritePolicy({
            exists: Aerospike.policy.exists.CREATE_OR_REPLACE
        }));
    }
    return client;
};

const aerospikeDriver = {
    set: async (key, value) => {
        const client = await getClient();
        return client.operate(globalKey, [
            Aerospike.maps.put('map', key, value)
        ]);
    },
    getAll: async () => {
        const client = await getClient();
        const result = await client.get(globalKey);
        return Object.keys(result.bins.map);
    }
};

module.exports = aerospikeDriver;
