const redis = require('redis');

let client = null;
const getClient  = () => {
   if (!client) {
       client = redis.createClient({ host: 'redis'});
   }
   return client;
};

const redisDriver = {
    set: async (key, value) => {
        return new Promise((resolve, reject) => {
            getClient().set(key, value, (err, res) => {
                if (err === null) {
                    resolve(res);
                    return;
                }

                reject(err);
            });
        });
    },
    getAll: async () => {
        return new Promise((resolve, reject) => {
            getClient().keys('*', (err, res) => {
                if (err === null) {
                    resolve(res.length);
                    return;
                }

                reject(err);
            });
        });
    }
};

module.exports = redisDriver;
