const http = require('http');
const drivers = require('./drivers');
const { v4: uuIdV4 } = require('uuid');


const server = http.createServer(({ url }, response) => {

	if (url === '/health') {
		response.writeHead(200);
		response.end('OK');
		return;
	}

	const key = uuIdV4();
	let driver = null;

    switch (url) {
    	case '/memory':
    		driver = 'memory';
    		break;
    	case '/redis':
			driver = 'redis';
    		break;
    	case '/mongodb':
			driver = 'mongodb';
    		break;
    	case '/memcached':
			driver = 'memcached';
    		break;
		case '/aerospike':
			driver = 'aerospike';
			break;
    	default:
    		response.writeHead(500);
    		response.end();
    		return;
    }

	drivers[driver].set(key, Date.now()).then(() => {
		return drivers[driver].getAll();
	}).then((documents) => {
		// const body = JSON.stringify(documents);
		const body = 'OK';

		response.writeHead(200, {
			'Content-Length': body.length,
			'Content-Type': 'text/plain'
		});

		response.end(body);
	}).catch((err) => {
		console.error('Error: ', err);
		response.writeHead(500);
		response.end();
	});
});

server.listen(8081);

console.log('Server is running on port 8081');
